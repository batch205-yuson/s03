//What is a class? - A class is a blueprint that describes an object in a non-specific way.
//Why use classes? - Using a class will make our code reusable.
//To create a class in JS, we use the class keyword and {}.
//Naming convention for clesses: Begins with Uppercase Characters.
/*Syntax
	class Name {
	
	};
*/

class Dog {
	
	//We add a constructor method to a class to be able to initialize values upon instantiating of an object from a class.
	//"Instantiation" is a technical term when creating an object from a class.
	//An object created from a class is called an instance.
	constructor(name,breed,age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}

};

//Instantiate a new object from the class
//new keyword is used to instantiate an object from a class
let dog1 = new Dog("Bantay", "chihuahua", 3);
console.log(dog1);

/*
	create a new class called Person.
	This person class should be able to instantiate a new object with the following fields:

	name,
	age,
	nationality,
	address,

	Instantiate 2 new objects from the Person class as person1 and person2

	log both objects in the console. Take a screenshot of your console and send it to the hangouts
*/

class Person {
	constructor(name,age,nationality,address){
		this.name = name;
		// this.age = age;
		this.nationality = nationality;
		this.address = address;	

		if(typeof age !== "number"){
			this.age = undefined;
		} else {
			this.age = age;
		}
	}

	greet(){
		console.log(`Hello! Good Morning!`);
		return this;
	};

	introduce(){
		//we should be able to return this, so that when used in a chain, we can still have an access to the this keyword
		console.log(`Hi! My Name is ${this.name}. I am from ${this.address}.`)
		return this;
	};

	changeAddress(newAddress){
		//We can also update the field of our object by referring to the property via this keyword and reassigning a new value.
		this.address = newAddress;
		return this;

	}
};

let person1 = new Person("Jett", 20, "Korean", "Korea");
console.table(person1);
let person2 = new Person("Reyna", 25, "Mexican", "Mexico");
console.table(person2);

//You can actually also chain methods from an instance
person1.greet().introduce();
person2.greet().introduce();


//Activity

class Student {
	constructor(name,email,grades){
		this.name = name;
		this.email = email;
		this.grades = grades;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		/*if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else {
			this.grades = grades
		} - solution*/

		if(typeof grades !== "array"){
			this.grades = grades;
		} else {
			this.grades = undefined;
		}
		
	};

	login(){
	    console.log(`${this.email} has logged in`);
	    return this;
	};
	logout(){
	     console.log(`${this.email} has logged out`);
	     return this;
	};
	listGrades(){
	    this.grades.forEach(grade => {
	        console.log(grade);

	    })
	    return this;

	};

	computeAve(){
		this.average = this.grades.reduce((a,b) => a+b, 0) / this.grades.length;
		console.log(this.average);
		return this;
	};

	willPass(){
		if(Math.round(this.average) >= 85) {
                this.isPassed = true
            } else {
            	this.isPassed = false
            }
		return this;
	};

	willPassWithHonors(){
		if(Math.round(this.average) >= 90){
                this.isPassedWithHonors = true
            } else if(Math.round(this.average) >= 85 && Math.round(this.average) < 90) {
                this.isPassedWithHonors = false
            } else {
                this.isPassedWithHonors = undefined
            };
            return this;
    };
};

let student1 = new Student("John","john@mail.com",[89, 84, 78, 88]);
let student2 = new Student("Joe","joe@mail.com",[78, 82, 79, 85]);
let student3 = new Student("Jane","jane@mail.com",[87, 89, 91, 93]);
let student4 = new Student("Jessie","jessie@mail.com",[91, 89, 92, 93]);

student1.login().listGrades().computeAve().willPass().willPassWithHonors()
student2.login().listGrades().computeAve().willPass().willPassWithHonors()
student3.login().listGrades().computeAve().willPass().willPassWithHonors()
student4.login().listGrades().computeAve().willPass().willPassWithHonors()

console.table(student1);
console.table(student2);
console.table(student3);
console.table(student4);